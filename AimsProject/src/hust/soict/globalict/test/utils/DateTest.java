package hust.soict.globalict.test.utils;

import hust.soict.globalict.aims.utils.DateUtils;
import hust.soict.globalict.aims.utils.MyDate;

public class DateTest {
    public static void main(String[] args) {
        /*
        MyDate myDate4 = new MyDate();
        MyDate.print(myDate4);
        MyDate myDate1 = new MyDate(2,3,2022);
        MyDate.print(myDate1);
        MyDate myDate2 = new MyDate("9/1/2002");
        MyDate.print(myDate2);
        MyDate myDate3 = MyDate.accept();
        MyDate.print(myDate3);
         */

        MyDate myDate1 = new MyDate(9, 1, 2002);
        MyDate myDate2 = new MyDate(30, 4, 1945);
        MyDate myDate3 = new MyDate(19,10,2030);

        myDate1.print(myDate1);
        myDate2.print(myDate2);
        myDate3.print(myDate3);

        DateUtils.sortDate(myDate1, myDate2, myDate3);

        System.out.println("Date after sorted: ");
        myDate1.print(myDate1);
        myDate2.print(myDate2);
        myDate3.print(myDate3);
    }
}
