package hust.soict.globalict.test.media;

import hust.soict.globalict.aims.media.Book;

import java.util.ArrayList;
import java.util.List;

public class BookTest {
    public static void main(String[] args) {
        List<Book> books = new ArrayList<>();
        books.add(new Book("Book1", "Cate1", 4.5f));
        books.add(new Book("Book2", "Cate2", 1.9f));
        books.add(new Book("Book3", "Cate3", 84));

        books.get(0).setContent("hello how are u today?");
        books.get(1).setContent("my name is Tracy he he he");
        books.get(2).setContent("a b c d e f g h i j k l m n p");

        System.out.println("Books:");
        for (Book book: books)
        {
            // toString() method
            System.out.println(book);
            System.out.println();
        }
    }
}
