package hust.soict.globalict.test.media;

import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Track;

import java.util.*;

public class TestMediaCompareTo {
    public static void main(String[] args) {

        System.out.println("DVD:");
        ArrayList<DigitalVideoDisc> dvds = new ArrayList<DigitalVideoDisc>();
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King", "Story", 19f, 19);
        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars", "Film", 5f, 79);
        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin", "Fiction", 6.7f, 22);

        // Add the DVD objects to the ArrayList
        dvds.add(dvd1);
        dvds.add(dvd2);
        dvds.add(dvd3);

        // Iterate through the ArrayList and output their titles
        // (unsorted order)
        Iterator<DigitalVideoDisc> DVDIterator = dvds.iterator();
        while (DVDIterator.hasNext())
        {
            DVDIterator.next().play();
        }
        DVDIterator = dvds.iterator();
        System.out.println("-----------------------------------------");
        System.out.println("The DVDs currently in the order are: ");
         while (DVDIterator.hasNext())
         {
             System.out.println(((DigitalVideoDisc)DVDIterator.next()).getTitle());
         }

         // use Collections.sort() with compareTo() method in Interface Comparable
        Collections.sort(dvds);

        // Iterate through the ArrayList and output their titles - in sorted order
        DVDIterator = dvds.iterator();
        System.out.println("-----------------------------------------");
        System.out.println("The DVDs in sorted order are: ");
        while (DVDIterator.hasNext())
        {
            System.out.println(DVDIterator.next().getTitle());
        }
        System.out.println("-----------------------------------------");


        System.out.println("CDs:");
        ArrayList<CompactDisc> cds = new ArrayList<CompactDisc>();
//        DigitalVideoDisc cd1 = new DigitalVideoDisc("The Lion King", "Story", 19f, 19);
//        DigitalVideoDisc cd2 = new DigitalVideoDisc("Star Wars", "Film", 5f, 79);
//        DigitalVideoDisc cd2 = new DigitalVideoDisc("Aladdin", "Fiction", 6.7f, 22);
        CompactDisc cd1 = new CompactDisc("CD1", "Cate1", 2, "Tracy");
        CompactDisc cd2 = new CompactDisc("CD2", "Cate2", 4.5f, "Terano");
        CompactDisc cd3 = new CompactDisc("CD3", "Cate3", 1.99f, "Bay");
        cd1.addTrack(new Track(34));
        cd2.addTrack(new Track(120));
        cd2.addTrack(new Track(19));
        cd3.addTrack(new Track(79));
        cd3.addTrack(new Track(58));

        // Add the CD objects to the ArrayList
        cds.add(cd1);
        cds.add(cd2);
        cds.add(cd3);

        // Iterate through the ArrayList and output their titles
        // (unsorted order)
        Iterator<CompactDisc> CDIterator = cds.iterator();
        while (CDIterator.hasNext())
        {
            CDIterator.next().play();
        }

        CDIterator = cds.iterator();
        System.out.println("-----------------------------------------");
        System.out.println("The CDs currently in the order are: ");
        while (CDIterator.hasNext())
        {
            System.out.println((CDIterator.next()).getTitle());
        }

        // use Collections.sort() with compareTo() method in Interface Comparable
        Collections.sort(cds);

        // Iterate through the ArrayList and output their titles - in sorted order
        CDIterator = cds.iterator();
        System.out.println("-----------------------------------------");
        System.out.println("The CDs in sorted order are: ");
        while (CDIterator.hasNext())
        {
            System.out.println((CDIterator.next()).getTitle());
        }
        System.out.println("-----------------------------------------");
    }
}
