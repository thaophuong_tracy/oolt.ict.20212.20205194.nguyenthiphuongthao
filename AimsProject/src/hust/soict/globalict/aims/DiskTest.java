//package hust.soict.globalict.aims;
//
//import java.util.Scanner;
//import hust.soict.globalict.aims.media.DigitalVideoDisc;
//import hust.soict.globalict.aims.order.Order;
//
//public class DiskTest {
//    public static void main(String[] args) {
//        // TODO Auto-generated method stub
//
//        Order anOrder = new Order();
//        // Create a new dvd object and set the fields
//
//        Scanner input = new Scanner(System.in);
//
//        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
//        dvd1.setCategory ("Animation");
//        dvd1.setCost(19.95f);
//        dvd1.setDirector("Roger Allers");
//        dvd1.setLength(87);
//
//        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
//        dvd2.setCategory ("Science Fiction");
//        dvd2.setCost(24.95f);
//        dvd2.setDirector("George Lucas");
//        dvd2.setLength(124);
//
//        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
//        dvd3.setCategory ("Animation");
//        dvd3.setCost(18.99f);
//        dvd3.setDirector("John Musker");
//        dvd3.setLength(90);
//
//
//        DigitalVideoDisc dvdList[] = {dvd1, dvd2, dvd3};
//        anOrder.addDigitalVideoDisc((dvdList));
//
//        // randomly pick out an item for free and print it out specifying
//        anOrder.printOrderFree();
//
//        System.out.println("Check dvd2");
//        System.out.println("Input title want to check in dvd2: ");
//        String title = input.nextLine();
//        System.out.println(dvd2.search(title));
//    }
//}
//
//
