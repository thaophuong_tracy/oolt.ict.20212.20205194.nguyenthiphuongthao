package hust.soict.globalict.aims.order;

import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.utils.MyDate;
import java.util.ArrayList;

public class Order {
    public static final int MAX_NUMBERS_ORDERED = 10;
    public static final int MAX_LIMITED_ORDERS = 5;

    // recreate itemsOrdered
    private ArrayList<Media> itemsOrdered = new ArrayList<Media>();

    public int id;
    private MyDate dateOrdered;
    private static int nbOrders = 0;

    public ArrayList<Media> getItemsOrdered() {
        return itemsOrdered;
    }

    public void setItemsOrdered(ArrayList<Media> itemsOrdered) {
        this.itemsOrdered = itemsOrdered;
    }

    // constructor method
    public Order(){
        MyDate date = new MyDate();
        nbOrders++;
        if (nbOrders <= MAX_LIMITED_ORDERS) {
            this.dateOrdered = date;
            this.id = nbOrders;
        }
        else
            System.out.println("It's over max limitation orders. Can not add anymore");
    }

    // addMedia() replace addDigitalVideoDisc()
    public void addMedia(Media media) {
        if(itemsOrdered.size() == MAX_NUMBERS_ORDERED)
            System.out.println("The order is full");
        else {
            media.setId(itemsOrdered.size() + 1);
            itemsOrdered.add(media);
            System.out.println("The media has been added");
        }
    }

    // removeMedia() replace removeDigitalVideoDisc()
    public void removeMedia(Media media) {
        if (itemsOrdered.size() == 0)
        {
            System.out.println("The order is empty");
            return;
        }
        int search = -1;
        for (int i = 0; i < itemsOrdered.size(); i++)
        {
            if (itemsOrdered.get(i).getTitle().equals(media.getTitle()))
                search = i;
        }
        if (search == -1)
            System.out.println("The media is not in list");
        else {
            itemsOrdered.remove(search);
            // reset id
            for (int i = search; i<itemsOrdered.size();i++)
            {
                itemsOrdered.get(i).setId(i+1);
            }
            System.out.println("The media is removed");
        }
    }

    // delete item by id
    public void removeMedia(int id) {
        if (itemsOrdered.size() == 0)
        {
            System.out.println("The order is empty");
            return;
        }
        int search = -1;
        for (int i = 0; i < itemsOrdered.size(); i++)
        {
            if (itemsOrdered.get(i).getId() == id)
                search = i;
        }
        if (search == -1)
            System.out.println("The media is not in list");
        else {
            itemsOrdered.remove(search);
            // reset id
            for (int i = search; i<itemsOrdered.size();i++)
            {
                itemsOrdered.get(i).setId(i+1);
            }
            System.out.println("The media is removed");
        }
    }

    // recreate total cost (not including lucky item)
    public float totalCost() {
        float sum = 0f;
        Media luckyItem = this.luckyItem();
        for (int i = 0; i < itemsOrdered.size(); i++){
//            if(itemsOrdered.get(i).equals(luckyItem))
//                System.out.println("Free item is: " + itemsOrdered.get(i).getTitle());
//            else
                sum += itemsOrdered.get(i).getCost();
        }
        return sum;
    }

    // recreate luckyItem()
    public Media luckyItem() {
        int rand = (int) (Math.random() * itemsOrdered.size());
        return itemsOrdered.get(rand);
    }

    // print order with lucky item
    public void printOrder() {
        if(itemsOrdered.size() == 0)
        {
            System.out.println("No item in the order");
            return;
        }
        System.out.println("*************************Order*************************************************************");
        System.out.println("Date: [" + dateOrdered.print() + "]");
        System.out.println("Ordered Items:");
        for (Media media : itemsOrdered) {
            System.out.println(media.getId()
                    + ". [" + media.getTitle() + "] - ["
                    + media.getCategory()
                    + "] - ["
                    + media.getCost() + "] $");
        }
        System.out.println("Total cost: " + this.totalCost() + " $");
        System.out.println("*******************************************************************************************");
    }

}
