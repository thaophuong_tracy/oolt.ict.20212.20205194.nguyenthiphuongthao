package hust.soict.globalict.aims.media;

import hust.soict.globalict.aims.utils.MemoryDaemon;

import java.lang.Object;
import java.util.Objects;

public abstract class Media implements Comparable<Object> {
    private String title;
    private String category;
    private float cost;
    private int id;

    public Media(String title) {
        this.title = title;
    }

    public Media(String title, String category)
    {
        this(title);
        this.category = category;
    }

    public Media(String title, String category, float cost)
    {
        this(title, category);
        this.cost = cost;
    }

    public Media() {

    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public float getCost() {
        return cost;
    }

    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            throw new NullPointerException("Object is null");
        }
        if (!(obj instanceof Media))
        {
            throw new ClassCastException("Not a media type");
        }
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Media otherMedia = (Media) obj;
        if (Float.floatToIntBits(cost) != Float.floatToIntBits(otherMedia.cost))
            return false;
        return true;
    }

    // compareTo() based on cost
    public int compareTo(Object obj)
    {
        // TODO Auto-generated method stub
        if (obj == null) {
        		throw new NullPointerException("Object is null");
        }
        if (!(obj instanceof Media)) {
            throw new ClassCastException("Not a media type");
        }
        final Media media = (Media) obj;
        if (!Objects.equals(this.title, media.title)) {
                   return this.title.compareTo(media.title);
        }
        if (this.id != media.id) {
            return this.id>media.id ? 1 : -1;
        }

        return 0;
    }
}
