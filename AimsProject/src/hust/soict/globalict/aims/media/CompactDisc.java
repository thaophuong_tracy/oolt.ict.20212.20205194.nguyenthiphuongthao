package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.Iterator;

import hust.soict.globalict.aims.PlayerException;

public class CompactDisc extends Disc implements Playable{
    private String artist;
    private ArrayList<Track> tracks = new ArrayList<Track>();

    public ArrayList<Track> getTracks() {
        return tracks;
    }

    public CompactDisc()
    {
        super();
    }

    public CompactDisc(String title)
    {
        super(title);
    }

    public CompactDisc(String title, String category)
    {
        super(title, category);
    }

    public CompactDisc(String title, String category, float cost)
    {
        super(title, category, cost);
    }

    public CompactDisc(String title, String category, float cost, String artist)
    {
        super(title, category, cost);
        this.artist = artist;
    }

    public CompactDisc(String title, String category, float cost, ArrayList<Track> tracks) {
        super(title, category, cost);
        this.tracks = tracks;
    }

    public String getArtist() {
        return artist;
    }

    public void addTrack(Track input)
    {
        if (tracks.contains(input))
        {
            System.out.println("Track is already input to list");
        }
        else {
            tracks.add(input);
        }
    }

    public void removeTrack(Track remove)
    {
        if (tracks.contains(remove))
        {
            tracks.remove(remove);
        } else
            System.out.println("Track is already input to list");
    }

    public int getLength()
    {
        int totalLength = 0;
        for (Track i:tracks)
        {
            totalLength += i.getLength();
        }
        return totalLength;
    }

    public void play() throws PlayerException{
        if(this.getLength() > 0) {
            // TODO Play all tracks in the CD as you have implemented
            Iterator<Track> iter = tracks.iterator();
            Track nextTrack;
            while(iter.hasNext()) {
                nextTrack = (Track) iter.next();
                try {
                    nextTrack.play();
                }catch(PlayerException e) {
                    throw e;
                }
            }
        } else {
            throw new PlayerException("ERROR: CD length is non-positive!");
        }
    }

    @Override
    public int compareTo(Object obj)
    {
        CompactDisc other = (CompactDisc) obj;
        if (tracks.size() != other.tracks.size())
            return tracks.size() - other.tracks.size();
        return getLength() - other.getLength();
    }
}
