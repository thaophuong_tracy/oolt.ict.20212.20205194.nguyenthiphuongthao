package hust.soict.globalict.aims.media;

import hust.soict.globalict.aims.PlayerException;
import hust.soict.globalict.aims.media.Media;

public class DigitalVideoDisc extends Disc implements Playable{
    public DigitalVideoDisc()
    {
        super();
    }

    public DigitalVideoDisc(String title) {
        super(title);
    }

    public DigitalVideoDisc(String title, String category) {
        super(title, category);
    }

    public DigitalVideoDisc(String title, String category, float cost) {
        super(title, category, cost);
    }

    public DigitalVideoDisc(String title, String category, float cost, int length) {
        super(title, category, cost, length);
    }

    public boolean search(String title)
    {
        return this.getTitle().toLowerCase().contains(title.toLowerCase());
    }

    public void play() throws PlayerException
    {
        if (this.getLength() >0) {
            System.out.println("Playing DVD: " + this.getTitle());
            System.out.println("DVD length: " + this.getLength());
        } else {
            throw new PlayerException("ERROR: DVD length is non-positive!");
        }
    }

    @Override
    public int compareTo(Object obj)
    {
        DigitalVideoDisc dvd = (DigitalVideoDisc) obj;
        if (this.getCost() == dvd.getCost())
            return 0;
        else if (this.getCost() < dvd.getCost())
            return 1;
        else
            return -1;
    }
}
