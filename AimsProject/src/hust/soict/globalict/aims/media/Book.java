package hust.soict.globalict.aims.media;

import java.util.*;

public class Book extends Media{
    private List<String> authors = new ArrayList<String>();

    private String content;
    private List<String> contentTokens;
    private Map<String,Integer> wordFrequency;

    public void setContent(String content) {
        this.content = content;
        processContent();
    }

    public Book() {}

    public Book(String title)
    {
        super(title);
    }

    public Book(String title, String category)
    {
        super(title, category);
    }

    public Book(String title, String category, float cost) {
        super(title, category, cost);
    }

    // ensure that author is not already in the ArrayList before adding
    public void addAuthor(String authorName) {
        for (String author: authors)
        {
            if (author.compareTo(authorName) == 0)
                return;
        }
        authors.add(authorName);
    }

    // ensure that author is present in the ArrayList before removing
    public void removeAuthor(String authorName) {
        for (String author: authors)
        {
            if (author.compareTo(authorName) == 0)
                authors.remove(author);
            break;
        }
    }

    public void processContent()
    {
        wordFrequency = new TreeMap<>();
        String[] arrayTokens = content.split("\\W+");

        for (String token: arrayTokens)
        {
            wordFrequency.putIfAbsent(token, 0);
            wordFrequency.put(token, (wordFrequency.get(token) + 1));
        }

        contentTokens = new ArrayList<String>(wordFrequency.keySet());
    }

    @Override
    public String toString()
    {
        String result = super.toString();
        result += "\nContent: " + content;
        result += "\nTokenList: " + contentTokens;
        result += "\nFrequency: " + wordFrequency;

        return result;
    }
}
