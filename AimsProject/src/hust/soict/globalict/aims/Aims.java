package hust.soict.globalict.aims;

import hust.soict.globalict.aims.media.*;
import hust.soict.globalict.aims.order.Order;
import hust.soict.globalict.aims.utils.MemoryDaemon;

import java.util.ArrayList;
import java.util.Scanner;

public class Aims {
    public static void showMenu() throws PlayerException {
        // list of order
        ArrayList<Order> orderList = new ArrayList<Order>();
        // each order is a list of Media
        ArrayList<Media> order = new ArrayList<Media>();
        Scanner input = new Scanner(System.in);

        int choice;
        do {
            System.out.println("Order Management Application: ");
            System.out.println("------------------------------------");
            System.out.println("1. Create new order");
            System.out.println("2. Add item to the order");
            System.out.println("3. Delete item by id");
            System.out.println("4. Display the items list of order");
            System.out.println("0. Exit");
            System.out.println("-------------------------------------");
            System.out.println("Please choose a number: 0-1-2-3-4");

            System.out.print("Your choice: ");

            choice = input.nextInt();
            input.nextLine();

            switch (choice) {
                case 1:
                    Order orderItem = new Order();
                    orderList.add(orderItem);
                    System.out.println("New order id: " + orderItem.id);
                    System.out.println("You already has created new order\n");
                    break;
                case 2:
                    System.out.println("Input id want to add: ");
                    int IDInput = input.nextInt();
                    input.nextLine();
                    int searchOrder = orderSearch(IDInput, orderList);
                    if (searchOrder == -1)
                    {
                        System.out.println("Wrong");
                        break;
                    }

                    System.out.println("Input number of item you want to add");
                    int numItem = input.nextInt();
                    input.nextLine();

                    for (int i = 0; i<numItem; i++){
                        System.out.println("Input type of item: ");
                        String type = input.nextLine();

                        System.out.print("Enter title: ");
                        String title = input.nextLine();

                        System.out.print("Enter category: ");
                        String category = input.nextLine();

                        System.out.print("Enter cost: ");
                        float cost = input.nextFloat();
                        input.nextLine();

                        switch (type) {
                            case "Book" -> {
                                Book newBook = new Book(title, category, cost);
                                orderList.get(searchOrder).addMedia(newBook);
                                //order.add(newBook);
                            }
                            case "CompactDisc" -> {
                                CompactDisc newMedia = new CompactDisc(title, category, cost);
                                System.out.println("Enter the number of tracks: ");
                                int numTrack = input.nextInt();
                                input.nextLine();

                                for (int j = 0; j < numTrack; j++) {
                                    System.out.println("Enter title: ");
                                    title = input.nextLine();
                                    System.out.println("Enter length: ");
                                    int length = input.nextInt();
                                    input.nextLine();
                                    Track newTrack = new Track(title, length);
                                    newMedia.addTrack(newTrack);
                                }
                                orderList.get(searchOrder).addMedia(newMedia);
                                System.out.println("Do you want to play the CD you just added?");
                                input = new Scanner(System.in);
                                String answer = input.nextLine();
                                if (answer.equals("Yes")) {
                                    newMedia.play();
                                }
                            }
                            case "DigitalVideoDisc" -> {
                                DigitalVideoDisc newMedia = new DigitalVideoDisc(title, category, cost);
                                orderList.get(searchOrder).addMedia(newMedia);
                                System.out.println("Do you want to play the CD you just added?");
                                input = new Scanner(System.in);
                                String answer = input.nextLine();
                                if (answer.equals("Yes")) {
                                    newMedia.play();
                                }
                            }
                        }
                    }
                    break;
                case 3:
                    System.out.print("What id of order you want to delete: ");
                    int IDOrder = input.nextInt();
                    input.nextLine();

                    int IDOrderSearch = orderSearch(IDOrder, orderList);
                    if (IDOrderSearch == -1)
                    {
                        System.out.println("Order is not exist");
                        break;
                    }

                    System.out.print("Ok! Input id of item you want to delete: ");
                    int idDelete = input.nextInt();
                    input.nextLine();

                    int IDSearch = IDSearch(idDelete, orderList.get(IDOrderSearch));
                    if (IDSearch == -1)
                    {
                        System.out.println("ID is not exist");
                        break;
                    }
                    orderList.get(IDOrderSearch).removeMedia(IDSearch + 1);
                    break;
                case 4:
                    System.out.println("Display list of ordering");
                    for(Order orderInList : orderList)
                    {
                        orderInList.printOrder();
                    }
                    break;
                case 0:
                    System.exit(0);
                    return;
            }
        } while(choice != 0);
    }

    public static int orderSearch(int id, ArrayList<Order> order)
    {
        for (int i = 0; i< order.size(); i++)
        {
            if(order.get(i).id == id)
                return i;
        }
        return -1;
    }

    public static int IDSearch(int id, Order order)
    {
        for (int i = 0; i< order.getItemsOrdered().size(); i++)
        {
            if(order.getItemsOrdered().get(i).getId() == id)
                return i;
        }
        return -1;
    }

    public static void main(String[] args) throws PlayerException {
        showMenu();

        MemoryDaemon memDaemon = new MemoryDaemon();
        Thread t1 = new Thread(memDaemon);
        t1.start();;
    }
}

