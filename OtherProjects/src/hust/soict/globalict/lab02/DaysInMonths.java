package hust.soict.globalict.lab02;

import java.util.Scanner;

public class DaysInMonths {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Input a year (must be a number): ");
        int year = scanner.nextInt();
        while (year < 1000) {
            System.out.print("That is an invalid year. Input a year again: ");
            year = scanner.nextInt();
        }
        boolean leapYear = (year % 4 == 0 || year % 100 == 0) || (year % 400 == 0);

        System.out.print("Input a month you want to know the number of days: ");
        String month = scanner.next();
        int day = 0;

        while (true) {
            switch (month) {
                case "January":
                case "Jan.":
                case "Jan":
                case "1":
                case "March":
                case "Mar.":
                case "Mar":
                case "3":
                case "May":
                case "5":
                case "July":
                case "Jul":
                case "7":
                case "August":
                case "Aug.":
                case "Aug":
                case "8":
                case "October":
                case "Oct.":
                case "Oct":
                case "10":
                case "December":
                case "Dec.":
                case "Dec":
                case "12":
                    day = 31;
                    break;
                case "April":
                case "Apr.":
                case "Apr":
                case "4":
                case "June":
                case "Jun":
                case "6":
                case "September":
                case "Sept.":
                case "Sep":
                case "9":
                case "November":
                case "Nov.":
                case "Nov":
                case "11":
                    day = 30;
                    break;
                case "February":
                case "Feb.":
                case "Feb":
                case "2":
                    day = leapYear ? 29 : 28;
                    break;
            }

            if (day != 0) {
                break;
            } else {
                System.out.print("That is an invalid month. Input a month again: ");
                month = scanner.next();
            }
        }

        System.out.println("Year " + year + " month " + month + " has " + day + " days.");
    }
}

/*
 * month == "January" || month == "Jan." || month == "Jan" || month == "1" ||
 * month == "February"
 * || month == "Feb." || month == "Feb" || month == "2" || month == "March" ||
 * month == "Mar."
 * || month == "Mar" || month == "3" || month == "April" || month == "Apr." ||
 * month == "January"
 * || month == "Apr" || month == "4" || month == "May" || month == "5" || month
 * == "June"
 * || month == "Jun" || month == "6" || month == "July" || month == "Jul" ||
 * month == "7"
 * || month == "August" || month == "Aug." || month == "Aug" || month == "8" ||
 * month == "September"
 * || month == "Sept." || month == "Sep" || month == "9" || month == "October"
 * || month == "Oct."
 * || month == "Oct" || month == "10" || month == "November" || month == "Nov."
 * || month == "Nov"
 * || month == "11" || month == "December" || month == "Dec." || month == "Dec"
 * || month == "12"
 */
