package hust.soict.globalict.lab02;

import java.util.Arrays;

public class AddTwoMatrices {
    public static void main(String args[]) {
        int[][] matricA = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
        int[][] matricB = { { 2, 4, 6 }, { 8, 1, 3 }, { 5, 7, 9 } };
        final int SIZE = 3;
        int[][] matricC = new int[SIZE][SIZE];

        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                matricC[i][j] = matricA[i][j] + matricB[i][j];
            }
        }

        System.out.println("Matric A:");
        System.out.println(Arrays.deepToString(matricA));
        System.out.println("Matric B:");
        System.out.println(Arrays.deepToString(matricB));
        System.out.println("Matric C = A + B");
        System.out.println(Arrays.deepToString(matricC));
    }
}

