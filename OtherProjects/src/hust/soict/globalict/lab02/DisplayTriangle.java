package hust.soict.globalict.lab02;

import java.util.Scanner;

public class DisplayTriangle {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input the height of the triangle: n = ");
        byte n = scanner.nextByte();
        for (int i = 1; i <= n; i++) {
            for (int z = i; z < n; z++) {
                System.out.print(' ');
            }
            for (int j = 1; j <= (i + i - 1); j++) {
                System.out.print('*');
            }
            System.out.print('\n');
        }
    }
}
