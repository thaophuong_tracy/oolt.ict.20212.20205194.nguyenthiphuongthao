package hust.soict.globalict.lab02;

import java.util.Arrays;
import java.util.Scanner;

public class SortArrays {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the length of array: ");
        int length = scanner.nextInt();

        int sum = 0;
        int[] array1 = new int[length];
        System.out.println("Input element of array:");
        for (int i = 0; i < length; i++) {
            array1[i] = scanner.nextInt();
            sum += array1[i];
        }

        for (int i = 0; i < length - 1; i++) {
            for (int j = i; j < length; j++) {
                if (array1[i] > array1[j]) {
                    int change = array1[i];
                    array1[i] = array1[j];
                    array1[j] = change;
                }
            }
        }

        System.out.println("Array after sorted:");
        System.out.println(Arrays.toString(array1));
        System.out.println("Sum of array: " + sum);
        System.out.println("Average value of array: " + sum / length);
    }
}

