package hust.soict.globalict.lab01;


// Example 5: CalTwoDoubles.java
import javax.swing.JOptionPane;

public class CalTwoDoubles {
    public static void main(String[] args) {
        String strNum1, strNum2;
        String result;

        strNum1 = JOptionPane.showInputDialog(null, "Please input the first number: ", "Input the first number",
                JOptionPane.INFORMATION_MESSAGE);
        double num1 = Double.parseDouble(strNum1);

        strNum2 = JOptionPane.showInputDialog(null, "Please input the second number: ", "Input the second number",
                JOptionPane.INFORMATION_MESSAGE);
        double num2 = Double.parseDouble(strNum2);

        double add = num1 + num2;
        double minus = num1 - num2;
        double multiple = num1 * num2;
        // double division = num1 / num2;
        result = "Sum: " + add + "\nDifference: " + minus + "\nProduct: " + multiple;
        // + "\nQuotient: " + division;

        double division;
        if (num2 == 0.0) {
            result += "\nQuotient: undefined";
        } else {
            division = num1 / num2;
            result += "\nQuotient: " + division;
        }

        JOptionPane.showMessageDialog(null, result, "Show results", JOptionPane.INFORMATION_MESSAGE);
        System.exit(0);

    }
}

