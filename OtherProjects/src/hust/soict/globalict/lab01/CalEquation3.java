package hust.soict.globalict.lab01;

import javax.swing.JOptionPane;

public class CalEquation3 {
    public static void main(String[] args) {
        String coA, coB, coC;
        String result;

        coA = JOptionPane.showInputDialog(null, "Please input a: ", "Input a", JOptionPane.INFORMATION_MESSAGE);
        double cA = Double.parseDouble(coA);

        coB = JOptionPane.showInputDialog(null, "Please input b: ", "Input b", JOptionPane.INFORMATION_MESSAGE);
        double cB = Double.parseDouble(coB);

        coC = JOptionPane.showInputDialog(null, "Please input c: ", "Input c", JOptionPane.INFORMATION_MESSAGE);
        double cC = Double.parseDouble(coC);

        double delta = cB * cB - 4 * cA * cC;
        if (delta == 0) {
            result = "The equation has double root " + (-cB / (2 * cA));
            JOptionPane.showMessageDialog(null, result, "Result", JOptionPane.INFORMATION_MESSAGE);
        } else if (delta > 0) {
            result = "The equation has two distinct roots " + ((-cB + Math.sqrt(delta)) / (2 * cA)) + " and "
                    + ((-cB - Math.sqrt(delta)) / (2 * cA));
            JOptionPane.showMessageDialog(null, result, "Result", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "The equation has no solution", "Result", JOptionPane.INFORMATION_MESSAGE);
        }
    }
}

