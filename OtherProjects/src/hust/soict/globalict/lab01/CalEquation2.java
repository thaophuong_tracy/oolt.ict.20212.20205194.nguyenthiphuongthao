package hust.soict.globalict.lab01;


//Example 6.2: First- degree equation with two variables
import javax.swing.JOptionPane;

public class CalEquation2 {
    public static void main(String[] args) {
        String coA11, coA12, coA21, coA22, coB1, coB2;
        String result;

        coA11 = JOptionPane.showInputDialog(null, "Please input A11: ", "Input A11", JOptionPane.INFORMATION_MESSAGE);
        double cA11 = Double.parseDouble(coA11);

        coA12 = JOptionPane.showInputDialog(null, "Please input A12: ", "Input A12",
                JOptionPane.INFORMATION_MESSAGE);
        double cA12 = Double.parseDouble(coA12);

        coB1 = JOptionPane.showInputDialog(null, "Please input B1: ", "Input B1",
                JOptionPane.INFORMATION_MESSAGE);
        double cB1 = Double.parseDouble(coB1);

        coA21 = JOptionPane.showInputDialog(null, "Please input A21: ", "Input A21",
                JOptionPane.INFORMATION_MESSAGE);
        double cA21 = Double.parseDouble(coA21);

        coA22 = JOptionPane.showInputDialog(null, "Please input A22: ", "Input A22",
                JOptionPane.INFORMATION_MESSAGE);
        double cA22 = Double.parseDouble(coA22);

        coB2 = JOptionPane.showInputDialog(null, "Please input B2: ", "Input B2",
                JOptionPane.INFORMATION_MESSAGE);
        double cB2 = Double.parseDouble(coB2);

        double D = cA11 * cA22 - cA21 * cA12;
        double D1 = cB1 * cA22 - cB2 * cA12;
        double D2 = cB2 * cA11 - cB1 * cA21;

        if (D != 0) {
            result = "The system has unique solution (x1, x2) = (" + D1 / D + "," + D2 / D + ")";
            JOptionPane.showMessageDialog(null, result, "Result", JOptionPane.INFORMATION_MESSAGE);
            System.exit(0);
        } else if (D1 == 0 && D2 == 0) {
            JOptionPane.showMessageDialog(null, "The system has infinitely many solutions", "Result",
                    JOptionPane.INFORMATION_MESSAGE);
            System.exit(0);
        } else {
            JOptionPane.showMessageDialog(null, "The system has no solution", "Result", JOptionPane.INFORMATION_MESSAGE);
            System.exit(0);
        }
    }
}

