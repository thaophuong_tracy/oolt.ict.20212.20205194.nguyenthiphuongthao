package hust.soict.globalict.lab01;

//Example 6.1: First- degree equation with one variable
import javax.swing.JOptionPane;

public class CalEquation {
    public static void main(String[] args) {
        String coeffA, coeffB;
        String result = "x = ";

        coeffA = JOptionPane.showInputDialog(null, "Please input the first coefficient: ", "Input the first coefficient",
                JOptionPane.INFORMATION_MESSAGE);
        double coA = Double.parseDouble(coeffA);

        coeffB = JOptionPane.showInputDialog(null, "Please input the second coefficient: ", "Input the second coefficient",
                JOptionPane.INFORMATION_MESSAGE);
        double coB = Double.parseDouble(coeffB);

        result += (-coB) / (coA);
        JOptionPane.showMessageDialog(null, result, "Result of equation: ", JOptionPane.INFORMATION_MESSAGE);
        System.exit(0);
    }
}

