package hust.soict.globalict.garbage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class readString {
    public static void main(String[] args) throws FileNotFoundException {
        System.out.println(new File(".").getAbsolutePath());
        File file = new File("longText.txt");
        Scanner input = new Scanner(file);

        while (input.hasNextLine())
            System.out.println(input.nextLine());

        input.close();
    }
}
